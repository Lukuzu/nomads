@extends('layouts.success')

@section('title', 'Success')

@section('content')
<main>
    <div class="section-success d-flex align-items-center">
        <div class="col text-center">
        <img src="{{ url('frontend/images/icon_mail.png') }}" alt="">
            <h1>Yey! Success</h1>
            <p>
                We've sent you email for trip
                <br>
                instruction please read it as well
            </p>
        <a href="{{ url('/')}}" class="btn btn-homepage mt-3 px-5">Homepage</a>
        </div>
    </div>

</main>    
@endsection